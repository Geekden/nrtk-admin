import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  apiKey: string = 'dfsf';

  constructor(public router: Router) { }

  ngOnInit() {
    if (this.apiKey) {
      console.log('overview');
      this.router.navigate(['overview']);
    } else {
      this.router.navigate(['login']);
    }
  }

  prepareRoute(outlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
